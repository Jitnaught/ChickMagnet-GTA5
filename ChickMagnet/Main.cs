﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Windows.Forms;

namespace ChickMagnet
{
    public class ChickMagnet : Script
    {
        Keys toggleKey;
        float radius, force, distanceMultiplier;
        bool ragdoll, setHealth;

        Ped[] nearbyPeds = new Ped[] { };
        int index = 0;
        bool enabled;

        public ChickMagnet()
        {
            enabled = Settings.GetValue("Settings", "Enabled_On_Startup", false);
            toggleKey = Settings.GetValue("Settings", "Toggle_Key", Keys.F8);
            radius = Settings.GetValue("Settings", "Radius", 100f);
            force = Settings.GetValue("Settings", "Force", 10f);
            distanceMultiplier = Settings.GetValue("Settings", "Distance_Multiplier", 0.3f);
            setHealth = Settings.GetValue("Settings", "Increase_Ped_Health", true);
            ragdoll = Settings.GetValue("Settings", "Ragdoll_Ped", true);

            KeyDown += ChickMagnet_KeyDown;
            Interval = 1;
            Tick += ChickMagnet_Tick;
        }

        private void ChickMagnet_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                var plrPed = Game.Player.Character;

                if (plrPed.Exists() && plrPed.IsAlive)
                {
                    if (index == nearbyPeds.Length)
                    {
                        nearbyPeds = World.GetNearbyPeds(plrPed, radius);
                        index = 0;
                    }

                    if (nearbyPeds.Length > 0)
                    {
                        Ped ped = nearbyPeds[index];

                        index++;

                        if (ped.Exists() && ped.IsAlive && Function.Call<int>(Hash.GET_PED_TYPE, ped) == 5) //is female
                        {
                            if (ragdoll)
                            {
                                Function.Call<bool>(Hash.SET_PED_TO_RAGDOLL, ped, 5000, 5000, 0, true, true, false);
                            }

                            if (setHealth) ped.Health = 1000;

                            Vector3 direction = Vector3.Normalize(plrPed.Position - ped.Position) * force * plrPed.Position.DistanceTo(ped.Position) * distanceMultiplier;

                            if (!plrPed.IsInVehicle() && Math.Abs(plrPed.Velocity.Z) > 1f && Math.Abs(direction.Z) > 0.5f) direction.Z = (direction.Z < 0.0f ? -0.5f : 0.5f); //limit Z force otherwise peds will push player up into the air

                            ped.ApplyForce(direction);
                        }
                    }
                }
                else if (nearbyPeds.Length != 0)
                {
                    nearbyPeds = new Ped[] { };
                    index = 0;
                }
            }
        }

        private void ChickMagnet_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == toggleKey)
            {
                enabled = !enabled;
                UI.ShowSubtitle($"Chick Magnet is now {(enabled ? "enabled" : "disabled")}");
            }
        }
    }
}
